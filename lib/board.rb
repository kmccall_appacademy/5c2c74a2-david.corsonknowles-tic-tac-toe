require 'byebug'

class Board
  attr_accessor :grid

  def initialize(arg = nil)
    @grid = arg || Array.new(3) { Array.new(3) }
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    grid[row][col] = mark
  end

  def over?
    !winner.nil? || grid.flatten.none? { |spot| spot.nil? }
  end

  def place_mark(pos, mark)
    # @grid[pos[0]][pos[1]] = mark
    self[pos] = mark
  end

  def empty?(pos)
    self[pos].nil?
  end

  def winner
    if won?(:X)
      return (:X)
    elsif won?(:O)
      return (:O)
    else
      return nil
    end
  end

  def to_s
    # grid.map {|row| row.join("_") }.join("\n")
    grid.map {|line| line.map { |spot| spot ? spot : spot = "_"}.join("") }.join("\n")

  end

  def empty_positions
    positions.select { |pos| self.empty?(pos) }
  end

  def would_win?(pos, mark)
    raise "Space is full" unless self[pos].nil?
    place_mark(pos, mark)
    would_win = (winner == mark)
    self[pos] = nil
    would_win
  end

  def valid?(move)
    move = parse_move(move)
    return false unless move.length == 2
    empty_positions.include?(move) && move.all? { |coord| (0..2).include?(coord) }
  end

  def parse_move(move)
    move.split(",").map(&:to_i)
    # arrayify(move).map { |e| e % 3 }
  end

  def arrayify(reply)
    result = []
    reply.chars.each {|entry| result << (entry.to_i) if "0123456789".include?(entry) }
    result
  end

  # private

  def positions
    places = []
    (3).times do |row|
      (3).times { |col| places << [row, col] }
    end
    places
  end

  def won?(mark)
    lines.any? do |line|
      line.all? { |pos| mark == self[pos] }
    end

  end

  def lines
    row_lines = (0..2).map do |y|
      [[0, y], [1, y], [2, y]]
    end

    column_lines = (0..2).map do |x|
      [[x, 0], [x, 1], [x, 2]]
    end

    diagonal_lines = [
      [[0, 0], [1, 1], [2, 2]],
      [[0, 2], [1, 1], [2, 0]]
    ]

    row_lines + column_lines + diagonal_lines
  end

end


# if __FILE__ == $PROGRAM_NAME
#   board = Board.new
#   puts board.to_s
#   puts
#   board.place_mark([0, 0], :X)
#   board.place_mark([1, 1], :O)
#   puts board.to_s
#   board.winner
# end
