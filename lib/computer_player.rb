class ComputerPlayer
  attr_accessor :name, :board, :mark

  def initialize(name)
    @name = name
    # @mark = [:X,:O][rand(1..2)]
  end

  def display(board)
    puts board.to_s
    @board = board
  end

  # get_move should return a winning move if one is available,
  # and otherwise move randomly
  def get_move
    board.empty_positions.each do |pos|
      return pos if board.would_win?(pos, mark)
    end
    board.empty_positions.sample
  end

end
