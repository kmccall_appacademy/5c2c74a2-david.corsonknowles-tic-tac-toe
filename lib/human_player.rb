class HumanPlayer
  attr_accessor :name, :board, :mark

  def initialize(name)
    @name = name
    # @mark = ( [:X,:O] - [ComputerPlayer.mark] ).first
  end

  def display(board)
    @board = board
    puts board.to_s
  end

  def get_move

    print "Where to next? Enter 0, 1 for example:  "
    move ||= gets.chomp
    # move.to_a
    # until @board.valid?(move)
    #   print "Please enter a valid move, ex. 2, 2 "
    #   move = gets.chomp
    # end
    parse_move(move)
  end

  private

  # reduce input errors with my arrayify method. together these methords
  # parse the first 2 digits and returns them in an array, mod 3.
  # extraneous digits are included in the array, and ignored by the move method
  def parse_move(move)
    # move.split(",").map(&:to_i)
    arrayify(move).map { |e| e % 3 }
  end

  def arrayify(reply)
    result = []
    reply.chars.each {|entry| result << (entry.to_i) if "0123456789".include?(entry) }
    result
  end

end
